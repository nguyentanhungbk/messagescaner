//
//  THCMainViewCtrl.m
//  THCMessageDectection
//
//  Created by Admin on 6/10/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "THCMainViewCtrl.h"
#import "THCMessageParser.h"

@interface THCMainViewCtrl () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextView *tvJsonArea;
@property (weak, nonatomic) IBOutlet UITextField *txtMessageView;

@property (nonatomic) THCMessageParser *scaner;

@end

@implementation THCMainViewCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.txtMessageView.delegate = self;
    self.tvJsonArea.text = @"";
    
    self.scaner = [[THCMessageParser alloc] init];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
}

- (void)keyboardWillHide:(NSNotification *)n
{
    [UIView animateWithDuration:1 animations:^{
        CGRect currentFrame = self.view.frame;
        CGRect newFrame = CGRectMake(0, 0, currentFrame.size.width, currentFrame.size.height);
        self.view.frame = newFrame;
    }];
}

- (void)keyboardWillShow:(NSNotification *)n
{
    
    NSDictionary* userInfo = [n userInfo];
    
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:1 animations:^{
        CGRect currentFrame = self.view.frame;
        CGRect newFrame = CGRectMake(0, -keyboardSize.height, currentFrame.size.width, currentFrame.size.height);
        self.view.frame = newFrame;
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendMessageAction:(UIButton *)sender {
    
    self.tvJsonArea.text = @"";
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *data = [self.scaner startParse:self.txtMessageView.text];
        NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSString *strResult = [str stringByReplacingOccurrencesOfString:@"\\" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, str.length)];
        
        // update UI on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tvJsonArea.text = [NSString stringWithFormat:@"%@",strResult];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
    });
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
    
}
@end
