//
//  THCScaner.m
//  THCMessageDectection
//
//  Created by Admin on 6/10/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "THCScaner.h"
#import "THCDefs.h"

@interface THCScaner ()
@end

@implementation THCScaner

- (NSArray *)startScan:(NSString *)message{
    
    NSArray *matches = [[NSArray alloc] init];;
    
    matches = [self.scaner matchesInString:message options:NSMatchingReportProgress range:NSMakeRange(0, message.length)];
    
    return matches;
}

@end
