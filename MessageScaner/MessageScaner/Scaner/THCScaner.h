//
//  THCScaner.h
//  THCMessageDectection
//
//  Created by Admin on 6/10/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface THCScaner : NSObject

@property (nonatomic) NSRegularExpression *scaner;

- (NSArray*)startScan:(NSString*)message;

@end
