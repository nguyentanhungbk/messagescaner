//
//  THCDefs.h
//  THCMessageDectection
//
//  Created by Admin on 6/10/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#ifndef THCMessageDectection_THCDefs_h
#define THCMessageDectection_THCDefs_h

#define MENTIONS_REGEX @"@[a-z|A-Z|0-9|_]+"
#define EMOTIONS_REGEX @"\\([a-z|A-Z|0-9]{1,15}\\)"
#define URL_REGEX @"(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w_\\.-]*)*\\/?"

#endif
