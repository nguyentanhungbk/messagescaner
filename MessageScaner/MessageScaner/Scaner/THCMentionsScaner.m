//
//  THCMentionsScaner.m
//  THCMessageDectection
//
//  Created by Admin on 6/10/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "THCMentionsScaner.h"
#import "THCDefs.h"

@implementation THCMentionsScaner

- (instancetype)init{
    self = [super init];
    if (self) {
        self.scaner = [[NSRegularExpression alloc] initWithPattern:MENTIONS_REGEX options:0 error:nil];
    }
    return self;
}

- (NSArray *)startScan:(NSString *)message{
    NSArray *baseResults = [super startScan:message];
    
    NSMutableArray *finalResults = [[NSMutableArray alloc] init];
    // custom data
    for (int i = 0; i < baseResults.count; i++) {
        
        NSTextCheckingResult *result = baseResults[i];
        NSString *mention = [message substringWithRange:result.range];
        [finalResults addObject:[mention substringFromIndex:1]];
        
    }
    return finalResults;
}

@end
