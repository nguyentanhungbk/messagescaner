//
//  THCUrlScaner.m
//  THCMessageDectection
//
//  Created by Admin on 6/10/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "THCUrlScaner.h"
#import "TFHpple.h"
#import "THCDefs.h"

@interface THCUrlScaner () <UIWebViewDelegate>
@property (nonatomic) UIWebView *webView;
@end

@implementation THCUrlScaner

- (instancetype)init{
    self = [super init];
    if (self) {
        self.scaner = [[NSRegularExpression alloc] initWithPattern:URL_REGEX options:0 error:nil];
    }
    return self;
}

- (NSArray *)startScan:(NSString *)message{
    NSArray *baseResults = [super startScan:message];
    
    NSMutableArray *linkArray = [[NSMutableArray alloc] init];
    
    if (baseResults.count > 0) {
        // get title of link
        for (int i = 0; i < baseResults.count; i++) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            NSTextCheckingResult *result = baseResults[i];
            NSString *urlStr = [message substringWithRange:result.range];
            
            TFHpple *hpple = [[TFHpple alloc] initWithHTMLData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]]];
            NSArray *t = [hpple searchWithXPathQuery:@"//title"];
            
            if (t.count > 0) {
                TFHppleElement *headerEle = t[0];
                [dic setObject:urlStr forKey:@"url"];
                [dic setObject:[headerEle text] forKey:@"title"];
                [linkArray addObject:dic];
            }
        }
    }
    
    return linkArray;
}

@end
