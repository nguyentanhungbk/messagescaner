//
//  THCMessageParser.m
//  THCMessageDectection
//
//  Created by Admin on 6/11/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "THCMessageParser.h"
#import "THCScaner.h"
#import "THCMentionsScaner.h"
#import "THCEmotionsScaner.h"
#import "THCUrlScaner.h"

@interface THCMessageParser ()

@property (nonatomic) THCScaner *mentionScaner;
@property (nonatomic) THCScaner *emotionScaner;
@property (nonatomic) THCScaner *urlScaner;

@end

@implementation THCMessageParser

- (instancetype)init {
    self = [super init];
    if (self) {
        self.mentionScaner = [[THCMentionsScaner alloc] init];
        self.emotionScaner = [[THCEmotionsScaner alloc] init];
        self.urlScaner = [[THCUrlScaner alloc] init];
    }
    return self;
}

- (NSData *)startParse:(NSString *)message{
    // scan for mentions
    NSArray *listMentions = [self.mentionScaner startScan:message];
    
    // scan for emotions
    NSArray *listEmotions = [self.emotionScaner startScan:message];
    
    // scan for url
    NSArray *listLinks = [self.urlScaner startScan:message];
    
    // create JSON
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    if (listMentions.count > 0) {
        [dic setObject:listMentions forKey:@"mentions"];
    }
    
    if (listEmotions.count > 0) {
        [dic setObject:listEmotions forKey:@"emoticons"];
    }
    
    if (listLinks.count > 0) {
        [dic setObject:listLinks forKey:@"links"];
    }
    
    // create json from data
    NSData *data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    
    return data;
}

@end
