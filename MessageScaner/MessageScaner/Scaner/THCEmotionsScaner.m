//
//  THCEmotionsScaner.m
//  THCMessageDectection
//
//  Created by Admin on 6/10/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "THCEmotionsScaner.h"
#import "THCDefs.h"

@implementation THCEmotionsScaner

- (instancetype)init{
    self = [super init];
    if (self) {
        self.scaner = [[NSRegularExpression alloc] initWithPattern:EMOTIONS_REGEX options:0 error:nil];
    }
    return self;
}

- (NSArray *)startScan:(NSString *)message{
    NSArray *baseResults = [super startScan:message];
    
    NSMutableArray *finalResults = [[NSMutableArray alloc] init];
    
    // custom data
    for (int i = 0; i < baseResults.count; i++) {
        
        NSTextCheckingResult *result = baseResults[i];
        NSString *emotion = [message substringWithRange:result.range];
        [finalResults addObject:[emotion substringWithRange:NSMakeRange(1, emotion.length-2)]];
        
    }

    return finalResults;
}

@end
